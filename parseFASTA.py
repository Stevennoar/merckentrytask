import argparse as args
DEFAULT_WRAP = 99999

class FASTANode:
    def __init__(self):
        self.Id = int()
        self.Description = str()
        self.Sequence = str()

    @staticmethod
    def DecodeFile(inputFilePath):
        fastaNodes = []
        fileString = open(inputFilePath).read()
        for dataBlock in fileString.split('>')[1:]:
            fastaNodes.append(FASTANode())
            for line in dataBlock.splitlines():
                if line == dataBlock.splitlines()[0]:
                    line = " ".join(line.split()).split(' ')
                    fastaNodes[-1].Id = line[0]
                    fastaNodes[-1].Description = ' '.join(line[1:len(line)])
                    print(f"ID: {fastaNodes[-1].Id}")
                    print(f"Description: {fastaNodes[-1].Description}")  
                else:
                    fastaNodes[-1].Sequence += line
            print(f"Sequence length: {len(fastaNodes[-1].Sequence)}\n")
        return fastaNodes

if __name__ == "__main__":
    parser = args.ArgumentParser()
    parser.add_argument("files", nargs='+', type=str, help="Input file sequence")
    parser.add_argument("--output", type=str, help="File to output FASTA from input files")
    parser.add_argument("--wrap", type=int, help="Line wrap string length")
    args = parser.parse_args()

    fastaNodes = []
    for file in args.files:
        print(f"File: {file}")
        fastaNodes.extend(FASTANode.DecodeFile(file))

    if args.output:
        for fastaNode in fastaNodes:
            with open(args.output, 'a') as file:
                file.write(f"> {fastaNode.Id} {fastaNode.Description}\n")
                wrap = args.wrap if args.wrap else DEFAULT_WRAP
                for i in range(0, len(fastaNode.Sequence), wrap):
                    file.write(f"{fastaNode.Sequence[i:i+wrap]}\n")
